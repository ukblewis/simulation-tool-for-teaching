# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/bvh" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHBoundingBoxVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHGroup.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHLineGeometry.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHLineSegmentVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHMotionTransform.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHNearestPointVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHNode.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHPageNode.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHPageRequest.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHPager.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHStaticBinary.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHStaticData.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHStaticGeometry.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHStaticGeometryBuilder.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHStaticLeaf.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHStaticNode.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHStaticTriangle.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHSubTreeCollector.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHMaterial.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHTransform.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/bvh/BVHVisitor.hxx"
    )
endif()

