# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/model" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/BoundingVolumeBuildVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/BVHDebugCollectVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/BVHPageNodeOSG.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/CheckSceneryVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/ConditionNode.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/ModelRegistry.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/PrimitiveCollector.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGClipGroup.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGInteractionAnimation.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGMaterialAnimation.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGPickAnimation.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGOffsetTransform.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGReaderWriterXML.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGRotateTransform.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGScaleTransform.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGText.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGTrackToAnimation.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/SGTranslateTransform.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/animation.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/model.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/modellib.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/particles.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/persparam.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/model/placement.hxx"
    )
endif()

