# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/tgdb" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/GroundLightManager.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/ReaderWriterSPT.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/ReaderWriterSTG.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGBuildingBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGDirectionalLightBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGLightBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGModelBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGNodeTriangles.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGOceanTile.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGReaderWriterBTG.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGTexturedTriangleBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGTileDetailsCallback.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGTileGeometryBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGTriangleBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGVasiDrawable.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/SGVertexArrayBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/ShaderGeometry.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/TreeBin.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/apt_signs.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/obj.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/pt_lights.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/tgdb/userdata.hxx"
    )
endif()

