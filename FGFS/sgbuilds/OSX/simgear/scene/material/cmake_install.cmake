# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/material" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/Effect.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/EffectBuilder.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/EffectCullVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/EffectGeode.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/Pass.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/Technique.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/TextureBuilder.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/mat.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/matlib.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/matmodel.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/mipmap.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/material/parseBlendFunc.hxx"
    )
endif()

