# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/util" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/ColorInterpolator.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/CopyOp.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/DeletionManager.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/NodeAndDrawableVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/Noise.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/OptionsReadFileCallback.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/OsgDebug.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/OsgMath.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/OsgSingleton.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/parse_color.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/PrimitiveUtils.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/QuadTreeBuilder.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/RenderConstants.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGDebugDrawCallback.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGEnlargeBoundingBox.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGNodeMasks.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGPickCallback.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGReaderWriterOptions.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGSceneFeatures.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGSceneUserData.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGStateAttributeVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGTextureStateAttributeVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SGUpdateVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/SplicingVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/StateAttributeFactory.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/UpdateOnceCallback.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/VectorArrayAdapter.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/scene/util/project.hxx"
    )
endif()

