# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/scene
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/scene
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(material)
subdirs(model)
subdirs(sky)
subdirs(tgdb)
subdirs(util)
subdirs(tsync)
