# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/structure

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/structure" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/OSGUtils.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/OSGVersion.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGAtomic.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGBinding.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGExpression.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGReferenced.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGSharedPtr.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGSmplhist.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGSmplstat.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGWeakPtr.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGWeakReferenced.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/SGPerfMon.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/singleton.hpp"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/Singleton.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/StringTable.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/callback.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/commands.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/event_mgr.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/exception.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/function_list.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/intern.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/map.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/subsystem_mgr.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/StateMachine.hxx"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/structure/detail" TYPE FILE FILES "/Users/blewis/SimulationTools/FGFS/simgear/simgear/structure/detail/function_list_template.hxx")
endif()

