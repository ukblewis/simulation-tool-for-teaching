# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/compiler.h"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/constants.h"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/sg_inlines.h"
    "/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/version.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/libSimGearCore.a")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libSimGearCore.a" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libSimGearCore.a")
    execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libSimGearCore.a")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/libSimGearScene.a")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libSimGearScene.a" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libSimGearScene.a")
    execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libSimGearScene.a")
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/bucket/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/bvh/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/debug/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/ephemeris/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/io/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/magvar/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/math/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/misc/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/nasal/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/nasal/cppbind/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/props/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/serial/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/structure/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/threads/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/timing/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/xml/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/package/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/environment/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/screen/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/scene/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/sound/cmake_install.cmake")

endif()

