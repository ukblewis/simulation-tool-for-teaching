# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/test_binobj.cxx" "/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/io/CMakeFiles/test_binobj.dir/test_binobj.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_CONFIG_H"
  "HAVE_EXPAT_CONFIG_H"
  "HAVE_INTTYPES_H"
  "XML_STATIC"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "simgear"
  "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/ShivaVG/include"
  "/Users/blewis/SimulationTools/FGFS/simgear"
  "."
  "/Users/blewis/SimulationTools/FGFS/simgear/3rdparty/expat"
  "3rdparty/expat"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include"
  "/usr/local/include"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenAL.framework/Headers"
  "/Users/blewis/SimulationTools/FGFS/simgear/3rdparty/utf8/source"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/CMakeFiles/SimGearScene.dir/DependInfo.cmake"
  "/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/CMakeFiles/SimGearCore.dir/DependInfo.cmake"
  )
