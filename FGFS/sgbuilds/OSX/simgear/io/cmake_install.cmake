# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/io

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/io" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/iochannel.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/lowlevel.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/raw_socket.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_binobj.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_file.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_netBuffer.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_netChannel.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_netChat.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_serial.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_socket.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/sg_socket_udp.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/HTTPClient.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/HTTPFileRequest.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/HTTPMemoryRequest.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/HTTPRequest.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/HTTPContentDecode.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/DAVMultiStatus.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/SVNRepository.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/SVNDirectory.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/io/SVNReportParser.hxx"
    )
endif()

