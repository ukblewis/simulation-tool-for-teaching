# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/misc

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/misc" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/CSSBorder.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/ListDiff.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/ResourceManager.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/SimpleMarkdown.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/SVGpreserveAspectRatio.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/interpolator.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/make_new.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/sg_dir.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/sg_path.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/sgstream.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/stdint.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/stopwatch.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/strutils.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/tabbed_values.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/texcoord.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/zfstream.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/misc/gzcontainerfile.hxx"
    )
endif()

