# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/misc
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/misc
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(CSSBorder "/test_CSSBorder")
add_test(tabbed_values "/test_tabbed_values")
add_test(streams "/test_streams")
add_test(path "/test_path")
add_test(SimpleMarkdown-boost_test "test-simgear_misc-SimpleMarkdown" "--catch_system_error=yes")
add_test(SVGpreserveAspectRatio-boost_test "test-simgear_misc-SVGpreserveAspectRatio" "--catch_system_error=yes")
add_test(strutils-boost_test "test-simgear_misc-strutils" "--catch_system_error=yes")
add_test(utf8tolatin1-boost_test "test-simgear_misc-utf8tolatin1" "--catch_system_error=yes")
