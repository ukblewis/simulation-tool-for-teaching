# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/math
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/math
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(math "/math_test")
add_test(geometry "/geometry_test")
