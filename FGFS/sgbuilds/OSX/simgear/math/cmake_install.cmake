# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/math

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/math" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGBox.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGCMath.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGGeoc.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGGeod.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGGeodesy.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGGeometry.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGGeometryFwd.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGIntersect.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGLimits.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGLineSegment.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGLocation.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGMath.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGMathFwd.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGMatrix.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGMisc.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGPlane.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGQuat.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGRay.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGRect.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGSphere.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGTriangle.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGVec2.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGVec3.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/SGVec4.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/beziercurve.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/interpolater.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/leastsqs.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/sg_geodesy.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/sg_types.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/math/sg_random.h"
    )
endif()

