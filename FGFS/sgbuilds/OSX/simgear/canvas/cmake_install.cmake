# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/canvas" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/canvas_fwd.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/Canvas.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasEvent.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasEventManager.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasEventTypes.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasEventVisitor.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasMgr.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasObjectPlacement.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasPlacement.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasSystemAdapter.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/CanvasWindow.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/ODGauge.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/VGInitOperation.hxx"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/ShivaVG/src/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/elements/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/events/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/layout/cmake_install.cmake")

endif()

