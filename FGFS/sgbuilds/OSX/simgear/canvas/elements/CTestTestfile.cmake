# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/elements
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/elements
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(canvas_element-boost_test "test-simgear_canvas_elements-canvas_element" "--catch_system_error=yes")
