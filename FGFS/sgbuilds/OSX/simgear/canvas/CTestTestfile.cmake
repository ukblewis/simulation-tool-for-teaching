# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(ShivaVG/src)
subdirs(elements)
subdirs(events)
subdirs(layout)
