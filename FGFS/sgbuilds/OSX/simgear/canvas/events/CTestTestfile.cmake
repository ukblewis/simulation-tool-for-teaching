# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/events
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/events
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(canvas_event-boost_test "test-simgear_canvas_events-canvas_event" "--catch_system_error=yes")
