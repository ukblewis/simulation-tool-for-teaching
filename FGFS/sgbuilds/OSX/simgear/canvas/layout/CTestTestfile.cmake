# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/canvas/layout
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/canvas/layout
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(canvas_layout-boost_test "test-simgear_canvas_layout-canvas_layout" "--catch_system_error=yes")
