# Install script for directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/ephemeris" TYPE FILE FILES
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/celestialBody.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/ephemeris.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/jupiter.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/mars.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/mercury.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/moonpos.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/neptune.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/pluto.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/saturn.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/star.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/stardata.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/uranus.hxx"
    "/Users/blewis/SimulationTools/FGFS/simgear/simgear/ephemeris/venus.hxx"
    )
endif()

