# CMake generated Testfile for 
# Source directory: /Users/blewis/SimulationTools/FGFS/simgear/simgear/props
# Build directory: /Users/blewis/SimulationTools/FGFS/sgbuilds/OSX/simgear/props
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(props "/test_props")
add_test(propertyObject "/test_propertyObject")
add_test(easing_functions "/test_easing_functions")
