# CMake generated Testfile for 
# Source directory: G:/Git/FGFS/simgear/simgear/nasal/cppbind
# Build directory: G:/Git/FGFS/sgbuilds/Windows/simgear/nasal/cppbind
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(cppbind_ghost-boost_test "test-simgear_nasal_cppbind-cppbind_ghost" "--catch_system_error=yes")
add_test(cppbind_misc-boost_test "test-simgear_nasal_cppbind-cppbind_misc" "--catch_system_error=yes")
add_test(nasal_gc_test-boost_test "test-simgear_nasal_cppbind-nasal_gc_test" "--catch_system_error=yes")
add_test(nasal_num-boost_test "test-simgear_nasal_cppbind-nasal_num" "--catch_system_error=yes")
