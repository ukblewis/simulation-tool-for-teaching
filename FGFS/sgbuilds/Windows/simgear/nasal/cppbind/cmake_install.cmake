# Install script for directory: G:/Git/FGFS/simgear/simgear/nasal/cppbind

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/nasal/cppbind" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/Ghost.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/NasalCallContext.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/NasalContext.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/NasalHash.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/NasalObject.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/NasalObjectHolder.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/NasalString.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/from_nasal.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/to_nasal.hxx"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/nasal/cppbind/detail" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/detail/from_nasal_function_templates.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/detail/from_nasal_helper.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/detail/functor_templates.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/detail/nasal_traits.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/detail/NasalObject_callMethod_templates.hxx"
    "G:/Git/FGFS/simgear/simgear/nasal/cppbind/detail/to_nasal_helper.hxx"
    )
endif()

