# Install script for directory: G:/Git/FGFS/simgear/simgear

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/compiler.h"
    "G:/Git/FGFS/simgear/simgear/constants.h"
    "G:/Git/FGFS/simgear/simgear/sg_inlines.h"
    "G:/Git/FGFS/sgbuilds/Windows/simgear/version.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/Debug/SimGearCored.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/Release/SimGearCore.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/MinSizeRel/SimGearCore.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/RelWithDebInfo/SimGearCore.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/Debug/SimGearScened.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/Release/SimGearScene.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/MinSizeRel/SimGearScene.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "G:/Git/FGFS/sgbuilds/Windows/simgear/RelWithDebInfo/SimGearScene.lib")
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/bucket/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/bvh/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/debug/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/ephemeris/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/io/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/magvar/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/math/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/misc/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/nasal/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/nasal/cppbind/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/props/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/serial/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/structure/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/threads/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/timing/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/xml/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/package/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/canvas/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/environment/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/screen/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/scene/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/sound/cmake_install.cmake")

endif()

