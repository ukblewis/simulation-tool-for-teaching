# CMake generated Testfile for 
# Source directory: G:/Git/FGFS/simgear/simgear/structure
# Build directory: G:/Git/FGFS/sgbuilds/Windows/simgear/structure
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(state_machine "/test_state_machine")
add_test(expressions "/test_expressions")
add_test(function_list-boost_test "test-simgear_structure-function_list" "--catch_system_error=yes")
add_test(shared_ptr-boost_test "test-simgear_structure-shared_ptr" "--catch_system_error=yes")
