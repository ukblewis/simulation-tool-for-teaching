# Install script for directory: G:/Git/FGFS/simgear/simgear/bvh

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/bvh" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/bvh/BVHBoundingBoxVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHGroup.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHLineGeometry.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHLineSegmentVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHMotionTransform.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHNearestPointVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHNode.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHPageNode.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHPageRequest.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHPager.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHStaticBinary.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHStaticData.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHStaticGeometry.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHStaticGeometryBuilder.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHStaticLeaf.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHStaticNode.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHStaticTriangle.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHSubTreeCollector.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHMaterial.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHTransform.hxx"
    "G:/Git/FGFS/simgear/simgear/bvh/BVHVisitor.hxx"
    )
endif()

