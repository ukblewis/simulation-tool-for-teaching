# CMake generated Testfile for 
# Source directory: G:/Git/FGFS/simgear/simgear
# Build directory: G:/Git/FGFS/sgbuilds/Windows/simgear
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(bucket)
subdirs(bvh)
subdirs(debug)
subdirs(ephemeris)
subdirs(io)
subdirs(magvar)
subdirs(math)
subdirs(misc)
subdirs(nasal)
subdirs(nasal/cppbind)
subdirs(props)
subdirs(serial)
subdirs(structure)
subdirs(threads)
subdirs(timing)
subdirs(xml)
subdirs(package)
subdirs(canvas)
subdirs(environment)
subdirs(screen)
subdirs(scene)
subdirs(sound)
