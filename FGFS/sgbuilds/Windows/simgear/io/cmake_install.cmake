# Install script for directory: G:/Git/FGFS/simgear/simgear/io

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/io" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/io/iochannel.hxx"
    "G:/Git/FGFS/simgear/simgear/io/lowlevel.hxx"
    "G:/Git/FGFS/simgear/simgear/io/raw_socket.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_binobj.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_file.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_netBuffer.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_netChannel.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_netChat.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_serial.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_socket.hxx"
    "G:/Git/FGFS/simgear/simgear/io/sg_socket_udp.hxx"
    "G:/Git/FGFS/simgear/simgear/io/HTTPClient.hxx"
    "G:/Git/FGFS/simgear/simgear/io/HTTPFileRequest.hxx"
    "G:/Git/FGFS/simgear/simgear/io/HTTPMemoryRequest.hxx"
    "G:/Git/FGFS/simgear/simgear/io/HTTPRequest.hxx"
    "G:/Git/FGFS/simgear/simgear/io/HTTPContentDecode.hxx"
    "G:/Git/FGFS/simgear/simgear/io/DAVMultiStatus.hxx"
    "G:/Git/FGFS/simgear/simgear/io/SVNRepository.hxx"
    "G:/Git/FGFS/simgear/simgear/io/SVNDirectory.hxx"
    "G:/Git/FGFS/simgear/simgear/io/SVNReportParser.hxx"
    )
endif()

