# Install script for directory: G:/Git/FGFS/simgear/simgear/canvas

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/canvas" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/canvas/canvas_fwd.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/Canvas.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasEvent.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasEventManager.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasEventTypes.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasEventVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasMgr.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasObjectPlacement.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasPlacement.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasSystemAdapter.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/CanvasWindow.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/ODGauge.hxx"
    "G:/Git/FGFS/simgear/simgear/canvas/VGInitOperation.hxx"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/canvas/ShivaVG/src/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/canvas/elements/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/canvas/events/cmake_install.cmake")
  include("G:/Git/FGFS/sgbuilds/Windows/simgear/canvas/layout/cmake_install.cmake")

endif()

