# Install script for directory: G:/Git/FGFS/simgear/simgear/scene/sky

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/sky" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/scene/sky/CloudShaderGeometry.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/cloud.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/cloudfield.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/dome.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/moon.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/newcloud.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/oursun.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/sky.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/sphere.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/sky/stars.hxx"
    )
endif()

