# Install script for directory: G:/Git/FGFS/simgear/simgear/scene/tgdb

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/tgdb" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/GroundLightManager.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/ReaderWriterSPT.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/ReaderWriterSTG.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGBuildingBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGDirectionalLightBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGLightBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGModelBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGNodeTriangles.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGOceanTile.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGReaderWriterBTG.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGTexturedTriangleBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGTileDetailsCallback.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGTileGeometryBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGTriangleBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGVasiDrawable.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/SGVertexArrayBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/ShaderGeometry.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/TreeBin.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/apt_signs.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/obj.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/pt_lights.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/tgdb/userdata.hxx"
    )
endif()

