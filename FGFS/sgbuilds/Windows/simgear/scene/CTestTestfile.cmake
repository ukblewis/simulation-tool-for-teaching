# CMake generated Testfile for 
# Source directory: G:/Git/FGFS/simgear/simgear/scene
# Build directory: G:/Git/FGFS/sgbuilds/Windows/simgear/scene
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(material)
subdirs(model)
subdirs(sky)
subdirs(tgdb)
subdirs(util)
subdirs(tsync)
