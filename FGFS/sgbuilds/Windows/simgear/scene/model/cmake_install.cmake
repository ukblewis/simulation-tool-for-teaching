# Install script for directory: G:/Git/FGFS/simgear/simgear/scene/model

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/model" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/scene/model/BoundingVolumeBuildVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/BVHDebugCollectVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/BVHPageNodeOSG.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/CheckSceneryVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/ConditionNode.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/ModelRegistry.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/PrimitiveCollector.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGClipGroup.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGInteractionAnimation.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGMaterialAnimation.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGPickAnimation.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGOffsetTransform.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGReaderWriterXML.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGRotateTransform.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGScaleTransform.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGText.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGTrackToAnimation.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/SGTranslateTransform.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/animation.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/model.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/modellib.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/particles.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/persparam.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/model/placement.hxx"
    )
endif()

