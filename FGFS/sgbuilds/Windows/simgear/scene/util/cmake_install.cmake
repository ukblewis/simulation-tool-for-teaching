# Install script for directory: G:/Git/FGFS/simgear/simgear/scene/util

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/scene/util" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/scene/util/ColorInterpolator.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/CopyOp.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/DeletionManager.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/NodeAndDrawableVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/Noise.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/OptionsReadFileCallback.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/OsgDebug.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/OsgMath.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/OsgSingleton.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/parse_color.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/PrimitiveUtils.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/QuadTreeBuilder.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/RenderConstants.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGDebugDrawCallback.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGEnlargeBoundingBox.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGNodeMasks.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGPickCallback.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGReaderWriterOptions.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGSceneFeatures.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGSceneUserData.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGStateAttributeVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGTextureStateAttributeVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SGUpdateVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/SplicingVisitor.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/StateAttributeFactory.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/UpdateOnceCallback.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/VectorArrayAdapter.hxx"
    "G:/Git/FGFS/simgear/simgear/scene/util/project.hxx"
    )
endif()

