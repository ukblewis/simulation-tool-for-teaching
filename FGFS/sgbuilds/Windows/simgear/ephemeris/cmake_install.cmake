# Install script for directory: G:/Git/FGFS/simgear/simgear/ephemeris

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/ephemeris" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/ephemeris/celestialBody.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/ephemeris.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/jupiter.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/mars.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/mercury.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/moonpos.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/neptune.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/pluto.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/saturn.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/star.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/stardata.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/uranus.hxx"
    "G:/Git/FGFS/simgear/simgear/ephemeris/venus.hxx"
    )
endif()

