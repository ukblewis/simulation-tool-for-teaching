# Install script for directory: G:/Git/FGFS/simgear/simgear/props

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/props" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/props/AtomicChangeListener.hxx"
    "G:/Git/FGFS/simgear/simgear/props/condition.hxx"
    "G:/Git/FGFS/simgear/simgear/props/easing_functions.hxx"
    "G:/Git/FGFS/simgear/simgear/props/ExtendedPropertyAdapter.hxx"
    "G:/Git/FGFS/simgear/simgear/props/PropertyBasedElement.hxx"
    "G:/Git/FGFS/simgear/simgear/props/PropertyBasedMgr.hxx"
    "G:/Git/FGFS/simgear/simgear/props/PropertyInterpolationMgr.hxx"
    "G:/Git/FGFS/simgear/simgear/props/PropertyInterpolator.hxx"
    "G:/Git/FGFS/simgear/simgear/props/propertyObject.hxx"
    "G:/Git/FGFS/simgear/simgear/props/props.hxx"
    "G:/Git/FGFS/simgear/simgear/props/props_io.hxx"
    "G:/Git/FGFS/simgear/simgear/props/propsfwd.hxx"
    "G:/Git/FGFS/simgear/simgear/props/tiedpropertylist.hxx"
    "G:/Git/FGFS/simgear/simgear/props/vectorPropTemplates.hxx"
    )
endif()

