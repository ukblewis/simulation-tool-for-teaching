# CMake generated Testfile for 
# Source directory: G:/Git/FGFS/simgear/simgear/props
# Build directory: G:/Git/FGFS/sgbuilds/Windows/simgear/props
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(props "/test_props")
add_test(propertyObject "/test_propertyObject")
add_test(easing_functions "/test_easing_functions")
