# Install script for directory: G:/Git/FGFS/simgear/simgear/math

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/SimGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/simgear/math" TYPE FILE FILES
    "G:/Git/FGFS/simgear/simgear/math/SGBox.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGCMath.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGGeoc.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGGeod.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGGeodesy.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGGeometry.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGGeometryFwd.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGIntersect.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGLimits.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGLineSegment.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGLocation.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGMath.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGMathFwd.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGMatrix.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGMisc.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGPlane.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGQuat.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGRay.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGRect.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGSphere.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGTriangle.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGVec2.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGVec3.hxx"
    "G:/Git/FGFS/simgear/simgear/math/SGVec4.hxx"
    "G:/Git/FGFS/simgear/simgear/math/beziercurve.hxx"
    "G:/Git/FGFS/simgear/simgear/math/interpolater.hxx"
    "G:/Git/FGFS/simgear/simgear/math/leastsqs.hxx"
    "G:/Git/FGFS/simgear/simgear/math/sg_geodesy.hxx"
    "G:/Git/FGFS/simgear/simgear/math/sg_types.hxx"
    "G:/Git/FGFS/simgear/simgear/math/sg_random.h"
    )
endif()

