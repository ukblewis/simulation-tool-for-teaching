# Install script for directory: G:/Git/FGFS/flightgear/utils

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/FlightGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("G:/Git/FGFS/fgbuilds/Windows/utils/fgadmin/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/utils/fgelev/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/utils/fgpanel/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/utils/fgviewer/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/utils/GPSsmooth/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/utils/TerraSync/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/utils/fgcom/cmake_install.cmake")

endif()

