# Install script for directory: G:/Git/FGFS/flightgear/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "G:/Git/FGFS/install/msvc100/FlightGear")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("G:/Git/FGFS/fgbuilds/Windows/src/Airports/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Aircraft/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/ATC/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Canvas/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Radio/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Autopilot/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Cockpit/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Environment/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/GUI/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Input/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Instrumentation/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Model/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/MultiPlayer/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/AIModel/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Navaids/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Network/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Scenery/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Scripting/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Sound/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Systems/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Time/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Traffic/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/FDM/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Viewer/cmake_install.cmake")
  include("G:/Git/FGFS/fgbuilds/Windows/src/Main/cmake_install.cmake")

endif()

