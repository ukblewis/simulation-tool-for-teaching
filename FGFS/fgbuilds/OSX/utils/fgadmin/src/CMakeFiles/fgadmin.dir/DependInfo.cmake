# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/blewis/SimulationTools/FGFS/flightgear/utils/fgadmin/src/untarka.c" "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/utils/fgadmin/src/CMakeFiles/fgadmin.dir/untarka.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_CONFIG_H"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/iaxclient/lib"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include"
  "/usr/local/include"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenGL.framework"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenAL.framework/Headers"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include/simgear/3rdparty/utf8"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/sqlite3"
  "/Users/blewis/SimulationTools/FGFS/flightgear"
  "/Users/blewis/SimulationTools/FGFS/flightgear/src"
  "src"
  "src/Include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/hts_engine_API/include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/flite_hts_engine/include"
  "utils/fgadmin/src"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/blewis/SimulationTools/FGFS/flightgear/utils/fgadmin/src/fgadmin_funcs.cxx" "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/utils/fgadmin/src/CMakeFiles/fgadmin.dir/fgadmin_funcs.cxx.o"
  "/Users/blewis/SimulationTools/FGFS/flightgear/utils/fgadmin/src/main.cxx" "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/utils/fgadmin/src/CMakeFiles/fgadmin.dir/main.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_CONFIG_H"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/iaxclient/lib"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include"
  "/usr/local/include"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenGL.framework"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenAL.framework/Headers"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include/simgear/3rdparty/utf8"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/sqlite3"
  "/Users/blewis/SimulationTools/FGFS/flightgear"
  "/Users/blewis/SimulationTools/FGFS/flightgear/src"
  "src"
  "src/Include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/hts_engine_API/include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/flite_hts_engine/include"
  "utils/fgadmin/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/utils/fgadmin/src/CMakeFiles/FGAdminUI.dir/DependInfo.cmake"
  )
