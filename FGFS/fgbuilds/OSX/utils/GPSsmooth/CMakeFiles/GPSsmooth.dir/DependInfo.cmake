# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/blewis/SimulationTools/FGFS/flightgear/utils/GPSsmooth/GPSsmooth.cxx" "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/utils/GPSsmooth/CMakeFiles/GPSsmooth.dir/GPSsmooth.cxx.o"
  "/Users/blewis/SimulationTools/FGFS/flightgear/utils/GPSsmooth/gps_main.cxx" "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/utils/GPSsmooth/CMakeFiles/GPSsmooth.dir/gps_main.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_CONFIG_H"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/iaxclient/lib"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include"
  "/usr/local/include"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenGL.framework"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenAL.framework/Headers"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include/simgear/3rdparty/utf8"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/sqlite3"
  "/Users/blewis/SimulationTools/FGFS/flightgear"
  "/Users/blewis/SimulationTools/FGFS/flightgear/src"
  "src"
  "src/Include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/hts_engine_API/include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/flite_hts_engine/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )
