IF(NOT EXISTS "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/install_manifest.txt")
    MESSAGE(FATAL_ERROR "Cannot find install manifest: \"/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/install_manifest.txt\"")
ENDIF()

FILE(READ "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/install_manifest.txt" files)
STRING(REGEX REPLACE "\n" ";" files "${files}")

FOREACH(file ${files})
    MESSAGE(STATUS "Uninstalling \"${file}\"")
    IF(EXISTS "${file}")
        EXEC_PROGRAM(
            "/usr/local/Cellar/cmake/3.3.2/bin/cmake" ARGS "-E remove \"${file}\""
            OUTPUT_VARIABLE rm_out
            RETURN_VALUE rm_retval
            )
        IF(NOT "${rm_retval}" STREQUAL 0)
            MESSAGE(FATAL_ERROR "Problem when removing \"${file}\"")
        ENDIF()
    ELSE()
        MESSAGE(STATUS "File \"${file}\" does not exist.")
    ENDIF()
ENDFOREACH()
