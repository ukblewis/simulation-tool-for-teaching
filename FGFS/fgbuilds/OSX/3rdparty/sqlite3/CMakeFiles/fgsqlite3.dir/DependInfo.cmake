# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/sqlite3/sqlite3.c" "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/3rdparty/sqlite3/CMakeFiles/fgsqlite3.dir/sqlite3.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_CONFIG_H"
  "NDEBUG"
  "SQLITE_OMIT_LOAD_EXTENSION"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/iaxclient/lib"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include"
  "/usr/local/include"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenGL.framework"
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk/System/Library/Frameworks/OpenAL.framework/Headers"
  "/Users/blewis/SimulationTools/FGFS/install/OSX/include/simgear/3rdparty/utf8"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/sqlite3"
  "/Users/blewis/SimulationTools/FGFS/flightgear"
  "/Users/blewis/SimulationTools/FGFS/flightgear/src"
  "src"
  "src/Include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/hts_engine_API/include"
  "/Users/blewis/SimulationTools/FGFS/flightgear/3rdparty/flite_hts_engine/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )
