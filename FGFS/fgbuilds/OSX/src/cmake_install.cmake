# Install script for directory: /Users/blewis/SimulationTools/FGFS/flightgear/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Airports/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Aircraft/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/ATC/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Canvas/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Radio/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Autopilot/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Cockpit/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Environment/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/GUI/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Input/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Instrumentation/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Model/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/MultiPlayer/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/AIModel/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Navaids/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Network/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Scenery/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Scripting/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Sound/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Systems/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Time/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Traffic/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/FDM/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Viewer/cmake_install.cmake")
  include("/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/src/Main/cmake_install.cmake")

endif()

