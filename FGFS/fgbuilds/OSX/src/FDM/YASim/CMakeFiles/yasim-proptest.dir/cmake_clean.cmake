file(REMOVE_RECURSE
  "CMakeFiles/yasim-proptest.dir/proptest.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Airplane.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Atmosphere.cpp.o"
  "CMakeFiles/yasim-proptest.dir/ControlMap.cpp.o"
  "CMakeFiles/yasim-proptest.dir/FGFDM.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Gear.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Glue.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Ground.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Hitch.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Hook.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Integrator.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Jet.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Launchbar.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Model.cpp.o"
  "CMakeFiles/yasim-proptest.dir/PistonEngine.cpp.o"
  "CMakeFiles/yasim-proptest.dir/PropEngine.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Propeller.cpp.o"
  "CMakeFiles/yasim-proptest.dir/RigidBody.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Rotor.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Rotorpart.cpp.o"
  "CMakeFiles/yasim-proptest.dir/SimpleJet.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Surface.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Thruster.cpp.o"
  "CMakeFiles/yasim-proptest.dir/TurbineEngine.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Turbulence.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Wing.cpp.o"
  "CMakeFiles/yasim-proptest.dir/Version.cpp.o"
  "yasim-proptest.pdb"
  "yasim-proptest"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/yasim-proptest.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
