file(REMOVE_RECURSE
  "CMakeFiles/yasim.dir/yasim-test.cpp.o"
  "CMakeFiles/yasim.dir/Airplane.cpp.o"
  "CMakeFiles/yasim.dir/Atmosphere.cpp.o"
  "CMakeFiles/yasim.dir/ControlMap.cpp.o"
  "CMakeFiles/yasim.dir/FGFDM.cpp.o"
  "CMakeFiles/yasim.dir/Gear.cpp.o"
  "CMakeFiles/yasim.dir/Glue.cpp.o"
  "CMakeFiles/yasim.dir/Ground.cpp.o"
  "CMakeFiles/yasim.dir/Hitch.cpp.o"
  "CMakeFiles/yasim.dir/Hook.cpp.o"
  "CMakeFiles/yasim.dir/Integrator.cpp.o"
  "CMakeFiles/yasim.dir/Jet.cpp.o"
  "CMakeFiles/yasim.dir/Launchbar.cpp.o"
  "CMakeFiles/yasim.dir/Model.cpp.o"
  "CMakeFiles/yasim.dir/PistonEngine.cpp.o"
  "CMakeFiles/yasim.dir/PropEngine.cpp.o"
  "CMakeFiles/yasim.dir/Propeller.cpp.o"
  "CMakeFiles/yasim.dir/RigidBody.cpp.o"
  "CMakeFiles/yasim.dir/Rotor.cpp.o"
  "CMakeFiles/yasim.dir/Rotorpart.cpp.o"
  "CMakeFiles/yasim.dir/SimpleJet.cpp.o"
  "CMakeFiles/yasim.dir/Surface.cpp.o"
  "CMakeFiles/yasim.dir/Thruster.cpp.o"
  "CMakeFiles/yasim.dir/TurbineEngine.cpp.o"
  "CMakeFiles/yasim.dir/Turbulence.cpp.o"
  "CMakeFiles/yasim.dir/Wing.cpp.o"
  "CMakeFiles/yasim.dir/Version.cpp.o"
  "yasim.pdb"
  "yasim"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/yasim.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
