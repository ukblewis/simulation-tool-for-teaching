# This file will be configured to contain variables for CPack. These variables
# should be set in the CMake list file of the project before CPack module is
# included. The list of available CPACK_xxx variables and their associated
# documentation may be obtained using
#  cpack --help-variable-list
#
# Some variables are common to all generators (e.g. CPACK_PACKAGE_NAME)
# and some are specific to a generator
# (e.g. CPACK_NSIS_EXTRA_INSTALL_COMMANDS). The generator specific variables
# usually begin with CPACK_<GENNAME>_xxxx.


SET(CPACK_BINARY_7Z "")
SET(CPACK_BINARY_BUNDLE "OFF")
SET(CPACK_BINARY_CYGWIN "")
SET(CPACK_BINARY_DEB "OFF")
SET(CPACK_BINARY_DRAGNDROP "OFF")
SET(CPACK_BINARY_IFW "OFF")
SET(CPACK_BINARY_NSIS "OFF")
SET(CPACK_BINARY_OSXX11 "OFF")
SET(CPACK_BINARY_PACKAGEMAKER "OFF")
SET(CPACK_BINARY_RPM "OFF")
SET(CPACK_BINARY_STGZ "ON")
SET(CPACK_BINARY_TBZ2 "OFF")
SET(CPACK_BINARY_TGZ "ON")
SET(CPACK_BINARY_TXZ "OFF")
SET(CPACK_BINARY_TZ "")
SET(CPACK_BINARY_WIX "")
SET(CPACK_BINARY_ZIP "")
SET(CPACK_CMAKE_GENERATOR "Unix Makefiles")
SET(CPACK_COMPONENTS_ALL "")
SET(CPACK_COMPONENT_UNSPECIFIED_HIDDEN "TRUE")
SET(CPACK_COMPONENT_UNSPECIFIED_REQUIRED "TRUE")
SET(CPACK_GENERATOR "TBZ2")
SET(CPACK_IGNORE_FILES ".*
Makefile
Debug/
Build
build
.*.swp
*.o
lib*.a
cmake_install.cmake
CMakeFiles
CMakeCache.txt
CPackConfig.cmake
CPackSourceConfig.cmake
cmake_uninstall.cmake
CTestTestfile.cmake
.kdev4
*.kdev4
*.pyc
;/Users/blewis/SimulationTools/FGFS/flightgear/.git;\\.gitignore")
SET(CPACK_INSTALLED_DIRECTORIES "/Users/blewis/SimulationTools/FGFS/flightgear;/")
SET(CPACK_INSTALL_CMAKE_PROJECTS "")
SET(CPACK_INSTALL_PREFIX "/Users/blewis/SimulationTools/FGFS/install/OSX")
SET(CPACK_MODULE_PATH "/Users/blewis/SimulationTools/FGFS/flightgear/CMakeModules;")
SET(CPACK_NSIS_DISPLAY_NAME "FlightGear 3.6.0")
SET(CPACK_NSIS_INSTALLER_ICON_CODE "")
SET(CPACK_NSIS_INSTALLER_MUI_ICON_CODE "")
SET(CPACK_NSIS_INSTALL_ROOT "$PROGRAMFILES")
SET(CPACK_NSIS_PACKAGE_NAME "FlightGear 3.6.0")
SET(CPACK_OSX_SYSROOT "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk")
SET(CPACK_OUTPUT_CONFIG_FILE "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/CPackConfig.cmake")
SET(CPACK_PACKAGE_DEFAULT_LOCATION "/")
SET(CPACK_PACKAGE_DESCRIPTION_FILE "/usr/local/Cellar/cmake/3.3.2/share/cmake/Templates/CPack.GenericDescription.txt")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "FlightGear built using CMake")
SET(CPACK_PACKAGE_FILE_NAME "flightgear-3.6.0")
SET(CPACK_PACKAGE_INSTALL_DIRECTORY "FlightGear 3.6.0")
SET(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "FlightGear 3.6.0")
SET(CPACK_PACKAGE_NAME "FlightGear")
SET(CPACK_PACKAGE_RELOCATABLE "true")
SET(CPACK_PACKAGE_VENDOR "Humanity")
SET(CPACK_PACKAGE_VERSION "3.6.0")
SET(CPACK_PACKAGE_VERSION_MAJOR "3")
SET(CPACK_PACKAGE_VERSION_MINOR "6")
SET(CPACK_PACKAGE_VERSION_PATCH "0")
SET(CPACK_RESOURCE_FILE_LICENSE "/Users/blewis/SimulationTools/FGFS/flightgear/COPYING")
SET(CPACK_RESOURCE_FILE_README "/Users/blewis/SimulationTools/FGFS/flightgear/README")
SET(CPACK_RESOURCE_FILE_WELCOME "/usr/local/Cellar/cmake/3.3.2/share/cmake/Templates/CPack.GenericWelcome.txt")
SET(CPACK_SET_DESTDIR "OFF")
SET(CPACK_SOURCE_7Z "")
SET(CPACK_SOURCE_CYGWIN "")
SET(CPACK_SOURCE_GENERATOR "TBZ2")
SET(CPACK_SOURCE_IGNORE_FILES ".*
Makefile
Debug/
Build
build
.*.swp
*.o
lib*.a
cmake_install.cmake
CMakeFiles
CMakeCache.txt
CPackConfig.cmake
CPackSourceConfig.cmake
cmake_uninstall.cmake
CTestTestfile.cmake
.kdev4
*.kdev4
*.pyc
;/Users/blewis/SimulationTools/FGFS/flightgear/.git;\\.gitignore")
SET(CPACK_SOURCE_INSTALLED_DIRECTORIES "/Users/blewis/SimulationTools/FGFS/flightgear;/")
SET(CPACK_SOURCE_OUTPUT_CONFIG_FILE "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/CPackSourceConfig.cmake")
SET(CPACK_SOURCE_PACKAGE_FILE_NAME "flightgear-3.6.0")
SET(CPACK_SOURCE_TBZ2 "")
SET(CPACK_SOURCE_TGZ "")
SET(CPACK_SOURCE_TOPLEVEL_TAG "Darwin-Source")
SET(CPACK_SOURCE_TXZ "")
SET(CPACK_SOURCE_TZ "")
SET(CPACK_SOURCE_ZIP "")
SET(CPACK_STRIP_FILES "")
SET(CPACK_SYSTEM_NAME "Darwin")
SET(CPACK_TOPLEVEL_TAG "Darwin-Source")
SET(CPACK_WIX_SIZEOF_VOID_P "8")

if(NOT CPACK_PROPERTIES_FILE)
  set(CPACK_PROPERTIES_FILE "/Users/blewis/SimulationTools/FGFS/fgbuilds/OSX/CPackProperties.cmake")
endif()

if(EXISTS ${CPACK_PROPERTIES_FILE})
  include(${CPACK_PROPERTIES_FILE})
endif()
