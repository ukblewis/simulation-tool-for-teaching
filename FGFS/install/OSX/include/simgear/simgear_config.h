
#define HAVE_SYS_TIME_H
#define HAVE_SYS_TIMEB_H
#define HAVE_UNISTD_H


#define HAVE_GETTIMEOFDAY
/* #undef HAVE_GETLOCALTIME */
#define HAVE_FTIME
#define HAVE_RINT
#define HAVE_TIMEGM
#define HAVE_ISNAN
#define HAVE_STD_ISNAN
/* #undef HAVE_WINDOWS_H */
#define HAVE_MKDTEMP

/* #undef GCC_ATOMIC_BUILTINS_FOUND */

/* #undef SYSTEM_EXPAT */
#define ENABLE_SOUND
