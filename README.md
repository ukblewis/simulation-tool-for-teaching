## PedagogicalSim

This application is targeted at academia and is a framework for constructing engineering lessons using FlightGear.

The software is made up of two code bases:
1. FlightGear
2. PedagogicalSim "tutor" software

In order to run the application, you simply need to run the executable in the folder ${APPLICATION_ROOT}/FGFSNodeLauncher/bin/PedagogicalSim.exe (for Windows) or ${APPLICATION_ROOT}/FGFSNodeLauncher/bin/PedagogicalSim.app (for OS X)

where ${APPLICATION_ROOT} is the location that you've download PedagogicalSim to.

Alternatively, shortcuts are provided in the root.

### Developing lessons

Releases of the application for Windows and OS X do not include the source code, so you must download from the "master" branch to develop new lessons.

If you would like to install a new aircraft model, you can place it inside ${APPLICATION_ROOT}/FGFS/fgdata/Aircraft/ ;
you may wish to add the aircraft to aircraftList.js in order to add details of target aircraft velocities, altitudes and positions in different flight conditions.

If you would like to modify the HUD, you can do so by modifying ${APPLICATION_ROOT}/FGFS/fgdata/Huds/custom.xml


In order to create new lessons, develop the lesson in HTML using Bootstrap for the UI and save the file to ${APPLICATION_ROOT}/FGFSNodeLauncher/views/LESSON_NAME.hbs

Then add the page to the application by adding the route to ${APPLICATION_ROOT}/routes/index.js using the following code:
`router.get('/LESSON_NAME', function(req, res, next) {
     res.render('LESSON_NAME', {path: 'LESSON_NAME', title: 'LESSON_NAME' +
     'PedagogicalSim', systemInfo: systemInfo(res) });
 });`

 If you wish to utilise FlightGear in your lesson, you can use the existing functions. You should base your page on ${APPLICATION_ROOT}/views/development.hbs since this provides an example of all of the functionality.

 If you have any additional development questions, contact Ben Lewis at [hello@blewis.co.uk](mailto:hello@blewis.co.uk)