var jsxml = require("node-jsxml");
var FileIO = require("./fileIO.js");
var fileIO = new FileIO();

var Namespace = jsxml.Namespace,
    QName = jsxml.QName,
    XML = jsxml.XML,
    XMLList = jsxml.XMLList;

module.exports = XMLIO;

/**
 * XMLIO helper object
 * @constructor
 */
function XMLIO() {}

/**
 * Helper function to read XML
 * @param {string} filename Full path of XML file to read
 * @param {engine.Socket} socket Socket.IO socket to write errors to
 * @returns {Promise} Promise will fulfill with JS XML object if successful or reject if failed
 */
XMLIO.prototype.readXML = function(filename,socket){
    return new Promise(function (fulfill, reject) {
        fileIO.readFile(filename, 'utf8').then(function (data) {
            fulfill(new XML(data));
        }).catch(
            // Log the rejection reason
            function (err) {
                console.log('Error occurred while parsing XML, error data: ' + err + '.');
                if (socket) {
                    socket.emit('error', err.toString());
                }
                reject(err);
            });
    });
};

/**
 * Helper function to write XML
 * @param {string} filename Full path of XML file to write to
 * @param {XML} XML JS XML object to write to file
 * @returns {Promise} Promise will fulfill if successful or reject if error writing to file
 */
XMLIO.prototype.writeXML = function(filename,XML){
    return new Promise(function (fulfill, reject) {
        return fileIO.writeFile(filename,XML.toXMLString()).then(function (written) {
            fulfill(written);
        }).catch(
            // Log the rejection reason
            function (err) {
                reject(err);
            });
    });
};