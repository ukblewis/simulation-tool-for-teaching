// Include the NodeJS library for handling paths
var path = require('path');

module.exports = FGFSPaths;

/**
 * FGFSPaths helper object
 * @constructor
 */
function FGFSPaths() {
    /**
     * Check if running on Windows OS
     * @type {boolean}
     */
    this.isWin = /^win/.test(process.platform);

    /* Initialise variables for launching FlightGear */
    /**
     * Path to FlightGear from FGFS folder depending on OS
     * @type {string}
     */
    this.pathToFlightGear = "";
    /**
     * Path to FlightGear data folder (fgdata)
     * @type {string}
     */
    this.pathToFGData = "fgdata";
    /**
     * FlightGear executable extension (if necessary)
     * @type {string}
     */
    this.OSExt = "";
    /**
     * FGFS path
     * @type {string}
     */
    this.FGFS = "";
    /**
     * Git root directory path
     * @type {string}
     */
    this.gitRootDir = "";

    // Build paths to FlightGear
    if (this.isWin) {
        this.pathToFlightGear = "install\\msvc100\\FlightGear\\bin";
        this.OSExt = ".exe";
        this.FGFS = "\\FGFS\\";
        this.gitRootDir = path.dirname(path.dirname((path.dirname(process.argv[1]))));
    }
    else {
        this.pathToFlightGear = "install/OSX/fgfs.app/Contents/MacOS";
        this.libraryDir = "install/OSX/fgfs.app/Contents/Library";
        this.OSExt = "";
        this.FGFS = "/FGFS/";
        this.gitRootDir = path.dirname(path.dirname((path.dirname(process.argv[1]))));
    }

}