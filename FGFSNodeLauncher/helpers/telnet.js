var net = require('net');

module.exports = Telnet;

/**
 * Telnet helper object
 * @param port Optional variable to choose Telnet port
 * @constructor
 */
function Telnet(port) {
    // Initialise and set necessary variables for telnet
    this.conn;
    this.Port = port | 5403;
}

/**
 * Helper function to connect via telnet to FlightGear Property Tree
 * @returns {Promise} Promise will fulfill with telnet connection handle if successful, or reject if failed
 * to retrieve handle
 */
Telnet.prototype.Connect = function() {
    return new Promise(function (fulfill, reject) {
        if (!this.conn || (this.conn && this.conn.destroyed == true)) {
            this.conn = net.connect(this.Port,'localhost',function () {
                console.log('telnet connected');
                this.conn.ready = true;
                fulfill(this.conn);
            }.bind(this));
            this.conn.on("error", function (c) {
                if (c.code == "ECONNREFUSED") {
                    // If the connection is refused, wait a second for telnet to open, then retry connecting
                    setTimeout(function() {
                        this.Connect().then(fulfill).catch(reject);
                    }.bind(this),500);
                }
                else {
                    console.log('telnet connection error: '+c.toString());
                    reject(c.toString());
                }
            }.bind(this));
        }
        else {
            fulfill(this.conn);
        }
    }.bind(this));
};

/**
 * Helper function to write or read from telnet connection
 * @param {string} cmd String to write to telnet connection (newline will be added)
 * @returns {Promise} Promise will fulfill with response from telnet (if any), or reject if connection error
 * encountered or connection isn't active/ready
 */
Telnet.prototype.WriteRead = function(cmd) {
    return new Promise(function (fulfill, reject) {
        if (this.conn && this.conn.ready) {
            // Write to telnet connection
            this.conn.write(cmd+'\r\n','utf8');
            var data = "";
            if (this.conn._events.data && this.conn._events.data.name == "processResponse") {
                this.conn._events.data = processResponse;
            }
            else {
                this.conn.on("data", processResponse);
            }
            this.conn._events.error = rejectError;
        }
        else {
            reject("Either telnet connection not active or not ready");
        }
        function processResponse(c) {
            // Reading telnet data to string
            data = c.toString();
            if (data != "") {
                var resultObject = {};
                var dataLines = data.split('\r\n');
                dataLines.forEach(function(dataLine) {
                    // Check this is a valid response
                    if (/(.*?) =/.test(dataLine) && /'(.*?)'/.test(dataLine)) {
                        // Get property name
                        var propName = dataLine.match(/(.*?) =/)[1].replace(/^(\/> )/,"");
                        // Get property value
                        var res = dataLine.match(/'(.*?)'/)[1];
                        // If provided with a callback to run on complete data - run it
                        resultObject[propName] = res;
                    }
                });
                fulfill(resultObject);
            }
        }
        function rejectError(c) {
            reject(c.toString());
        }
    }.bind(this));
};