module.exports = FileIO;

var fs = require('fs');

/**
 * FileIO helper object
 * @constructor
 */
function FileIO() {}

/**
 * Helper function to read files
 * @param {string} filename Full path to the file to read
 * @returns {Promise} Promise will fulfill with data or reject with error
 */
FileIO.prototype.readFile = function(filename){
    return new Promise(function (fulfill, reject){
        fs.readFile(filename, 'utf8', function (err, res){
            if (err) reject(err);
            else fulfill(res.toString());
        });
    });
};

/**
 * Helper function to write files
 * @param {string} filename Full path to write data to
 * @param {string} string String containing data to write to file
 * @returns {Promise} Promise will fulfill if successful or reject if error writing to file
 */
FileIO.prototype.writeFile = function(filename,string){
    return new Promise(function (fulfill, reject){
        fs.writeFile(filename, string, 'utf8', function (err, res){
            if (err) reject(err);
            else fulfill(res);
        });
    });
};