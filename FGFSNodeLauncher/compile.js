/* This file is used to create a system executable for Windows or OS X using EncloseJS */
"use strict";

var isWin = /^win/.test(process.platform);

/* Update JSDoc documentation after creating executable */
var cp = require("child_process"),
    launchCommand = "./node_modules/.bin/jsdoc",
    launchProps = ["-c","conf.json"];
if (isWin) {
    launchCommand += ".cmd";
}
cp.spawn(launchCommand,launchProps);

/* Create the executable using EncloseJS */
var flags = [];
var enclose = require("enclose").exec;
// Load config containing list of files/folders to include in the executable
flags.push("--config", "./config.js");
// Choose appropriate file extension for Windows or OS X executables
if (isWin) {
    flags.push("-o","./bin/PedagogicalSim.exe");
}
else {
    flags.push("--x64"); // OS X builds are 64-bit
    flags.push("-o","./bin/PedagogicalSim.app");
}
// Build for all Windows/OS X machines, i.e. don't use CPU features; you can disable this if you will be
// running it on this computer
flags.push("--features=no");
// Add JS file to build into the executable
flags.push("./bin/www");
// Run EncloseJS
enclose(flags);