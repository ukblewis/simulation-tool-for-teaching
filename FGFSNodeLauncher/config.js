/* This is the configuration file for the compile.js file which creates system executables */
module.exports = {
    assets: ["./views/*.*",
        "./views/*/*.*",
        "./public/*.*",
        "./public/*/*.*",
        "./JSDoc/*.*",
        "./JSDoc/*/*.*",
        "./JSDoc/*/*/*.*",
        "./node_modules/bootstrap/dist/js/*.*",
        "./node_modules/bootstrap/dist/css/*.*",
        "./node_modules/bootstrap/less/*.*",
        "./node_modules/bootstrap/dist/fonts/*.*",
        "./node_modules/bootstrap-select/dist/css/*.*",
        "./node_modules/bootstrap-select/dist/js/*.*",
        "./node_modules/jquery/dist/*.*"
    ],
    scripts: "./helpers/*.js",
    dirs: [ "./views",
            "./views/constraints",
            "./views/joystick",
        "./templates" ]
};