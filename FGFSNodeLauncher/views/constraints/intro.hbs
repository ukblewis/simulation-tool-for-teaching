<link rel="stylesheet" href="/stylesheets/sidebar.css">
<script src="/javascripts/sidebar.js"></script>

<!-- Main component for a primary marketing message or call to action -->
<div class="jumbotron">
    <h1>{{title}}</h1>
    <p>
        In this section, you will learn about <b>Constraints Analysis</b>.
    </p>
</div>

<div class="col-md-9" role="main">
<div class="panel panel-default" id="wdwm">
    <div class="panel-heading">
        <h4>What do we mean by Constraints Analysis?</h4>
    </div>
    <div class="panel-body">
        <p>Constraints Analysis in preliminary aircraft design refers to the use of key aircraft flight profile
        conditions to constrain the loci of the allowable values for an aircraft's <b>Thrust to Weight ratio (T/W)</b>
        and <b>Wing Loading (W/S)</b>.</p>
        <ul class="list-group">
            <li class="list-group-item">
                The Thrust to Weight is defined as the ratio of the aircraft's <i>sea-level static</i> thrust (T) to
                it's <i>maximum takeoff</i> weight (MTOW).
            </li>
            <li class="list-group-item">
                The Wing Loading is defined as the ratio of the aircraft's <i>maximum takeoff</i> weight (MTOW) to it's
                wing <i>area</i> (S).
            </li>
        </ul>
    </div>
</div>
<div class="alert alert-info alert-dismissible" role="alert" id="whytwwl">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <b>Why</b> do you think that care about the aircraft's <b>Thrust to Weight ratio</b> and
    <b>Wing Loading</b>? <a href="#" onclick="document.getElementById('tw-wl-answer').style.display='block';
    this.style.display='none'; return false;">Click here for the answer</a>
    <div id="tw-wl-answer" style="display: none"><br>
        <ul class="list-group">
            <li class="list-group-item"><b>Aircraft design</b> is <b>driven</b> by <i>weight</i>, heavier aircraft are
                more expensive to build and more costly to fly because they require more powerful engines.</li>
            <li class="list-group-item">The <b>key variables</b> in an aircraft design are the <i>wing size</i> (since
                this provides the lift that keeps the aircraft flying) and the <i>engine size</i> (since this propels
                the aircraft forwards, and this velocity creates the flow over the wings which results in lift).</li>
            <li class="list-group-item">Wing size can be considered by wing area, and for a given aspect ratio, we can
                consider the lift.</li>
            <li class="list-group-item">Likewise, for engines, we can consider their sea-level static thrust provided.</li>
            <li class="list-group-item">In order to compare aircraft of different sizes, and because the design process
                normally begins with weight estimation and breakdown, the thrust provided by the engines and wing area
                are normalised against the weight, hence we use Thrust to Weight ratio, Thrust / Weight or T/W and
                Wing Loading, Wing Area / Weight or W/S.</li>
        </ul>
    </div>
</div>
<div class="panel panel-default" id="kcca">
    <div class="panel-heading">
        <h4>What are the key constraints for civilian aircraft design?</h4>
    </div>
    <div class="panel-body">
        <p>For a civilian aircraft, there are 4 key constraints, they are: </p>
        <ul class="list-group">
            <li class="list-group-item"><b>Takeoff</b>: An aircraft must have sufficiently large thrust and sufficiently large
                wing area to lift off.</li>
            <li class="list-group-item"><b>Climb</b>: An aircraft must have sufficiently large thrust to climb to cruise conditions.
            </li>
            <li class="list-group-item"><b>Cruise</b>: An aircraft must have sufficiently large thrust and sufficiently large
                wing area to sustain cruise flight conditions.</li>
            <li class="list-group-item"><b>Landing</b>: An aircraft must have a sufficiently small wing area that it can land.
            </li>
        </ul>
    </div>
</div>
<div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <p>
        Note: In order to create a Constraints Analysis for these key constraints, only a <b>few aircraft aerodynamic
        parameters</b> have to be <b>found and/or estimated</b>. The <b>most important</b> are the drag polar constants,
        i.e. the <b>coefficient of drag for zero-lift</b>, C<sub>d,0</sub> and the <b>coefficient of lift induced
        drag</b>, C<sub>d,i</sub> and the <b>maximum coefficient of lift</b>, C<sub>Lmax</sub>.
    </p>
</div>
<div class="panel panel-default" id="whereaircraftonconstraints">
    <div class="panel-heading">
        <h4>Where on a Constraints Analysis would you typically expect to find different aircraft?</h4>
    </div>
    <div class="panel-body">
        <img src="/images/ConstraintsOverview.png" alt="Commerical transports are in the bottom right,
        General aviation in the bottom left, above motor gliders, aerobatic aircraft are on the top left and fighter
        jet aircraft are on the top right" />
        <h6>Citation: 2nd Year Aircraft Design lecture slides</h6>
    </div>
</div>
<div class="panel panel-default" id="keq">
    <div class="panel-heading">
        <h4>What are the key Constraint Analysis equations?</h4>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            <li class="list-group-item"><b>Takeoff</b>: <img src="/images/TakeoffConstraint.svg" class="center-block"
                                                             alt="T/W = ( (1.1^2) / (C_Lmax_TO rho g S_TO)) (W/S)"/>
            </li>
            <li class="list-group-item"><b>Climb</b>: <img src="/images/ClimbConstraint.svg" class="center-block"
                                                           alt="T/W = gamma + 1 / (L/D)_climb"/>
            </li>
            <li class="list-group-item"><b>Cruise</b>: <img src="/images/CruiseConstraint.svg" class="center-block"
                                                            alt="T/W = beta / alpha ( (A q_c) / (W/S)
                                                            + beta kappa /q_c (W/S) )"/>
            </li>
            <li class="list-group-item"><b>Landing</b>: <img src="/images/LandingConstraint.svg" class="center-block"
                                                             alt="W/S = ( 0.6(S_L - 300) mu g rho C_Lmax_L ) / 1.2^2"/>
            </li>
        </ul>
        <p>
            Note: You don't need to memorise these equations, you will either be given them or expected to derive them.
        </p>
    </div>
</div>
<div class="alert alert-info alert-dismissible" role="alert" id="weau">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    Which equations are used to derive the Constraint Analysis equations?
    <a href="#" onclick="document.getElementById('ca-se-answer').style.display='block'; this.style.display='none';
    return false;">Click here for the answer</a>
    <div id="ca-se-answer" style="display: none"><br>
        <ul class="list-group">
            <li class="list-group-item"><b>Takeoff distance</b>: <img src="/images/TakeoffDistance.svg"
                                                                      class="center-block"
                                                                      alt="S_{TO} = (V_{TO})^2 / ( 2 a_{TO}"/>
                <br>
                (From dynamics, assuming linear motion)
            </li>
            <li class="list-group-item"><b>Landing distance</b>: <img src="/images/LandingDistance.svg"
                                                                      class="center-block"
                                                                      alt="S_L = (V_L)^2 / 2 a_L"/>
                <br>
                (From dynamics, assuming linear motion)
            </li>
            <li class="list-group-item"><b>Takeoff acceleration</b>: <img src="/images/TakeoffAcceleration.svg"
                                                                          class="center-block"
                                                                          alt="a_{TO} = ( T / W ) g"/>
                <br>
                (From dynamics, Newtons third law and definition of weight)
            </li>
            <li class="list-group-item"><b>Landing deceleration</b>: <img src="/images/LandingDeceleration.svg"
                                                                          class="center-block"
                                                                          alt="a_L = mu g"/>
                <br>
                (From dynamics, specifically friction)
            </li>
            <li class="list-group-item"><b>Lift coefficent</b>: <img src="/images/LiftCoefficient.svg"
                                                                          class="center-block"
                                                                          alt="C_L = ( 2 / (rho V^2) ) ( W / S )"/>
                <br>
                (From aerodynamics)
            </li>
            <li class="list-group-item"><b>Landing speed</b>: <img src="/images/LandingSpeed.svg"
                                                                     class="center-block"
                                                                     alt="V_L = 1.2 sqrt( (2 / (rho C_{L_{L}}) ) ( W / S
                                                                      ) )"/>
                <br>
                (From assuming L = W and using the lift coefficient equation; 1.2 is due to landing speed being 1.2x
                stall speed)
            </li>
            <li class="list-group-item"><b>Takeoff speed</b>: <img src="/images/TakeoffSpeed.svg"
                                                                   class="center-block"
                                                                   alt="V_{TO} = 1.1 sqrt( (2 / (rho C_{L_{TO}}) )
                                                                   ( W / S ) )"/>
                <br>
                (From assuming L = W and using the lift coefficient equation; 1.1 is due to takeoff speed being 1.2x
                stall speed)
            </li>
            <li class="list-group-item"><b>Drag polar</b>: <img src="/images/DragPolar.svg"
                                                                   class="center-block"
                                                                   alt="C_D = C_{D,0} + kappa (C_L)^2"/>
                <br>
                (From aerodynamics)
            </li>
            <li class="list-group-item"><b>Cruise dynamic pressure</b>: <img src="/images/DynamicPressure.svg"
                                                                class="center-block"
                                                                alt="q_c = ( 1 / 2 ) rho_c (V_c)^2"/>
                <br>
                (From fluid dynamics/aerodynamics)
            </li>
            <li class="list-group-item"><b>Thrust lapse</b>: <img src="/images/ThrustLapse.svg"
                                                                             class="center-block"
                                                                             alt="T_{cruise} = alpha T_{sea level}"/>
                <br>
                (Indirectly from air density variation with altitude, variable alpha defined specifically for this
                purpose)
            </li>
            <li class="list-group-item"><b>Weight Fraction</b>: <img src="/images/WeightFraction.svg"
                                                                  class="center-block"
                                                                  alt="W_i = = beta W_0"/>
                <br>
                (From the weight breakdown)
            </li>
        </ul>
    </div>
</div>
<div class="panel panel-default" id="derive">
    <div class="panel-heading">
        <h4>How do we derive a Constraint Analysis equation?</h4>
    </div>
    <div class="panel-body">
        <img src="/images/CruiseEquationDerived.svg" alt="Start with the drag polar:
		C_D = C_{D,0} + kappa {C_L}^2
		Substitute the lift coefficient when assuming lift equals weight ( C_L = ((2)/(rho V^2)) (W/S) ) :
		C_D = C_{D,0} + kappa ( ((2) / (rho V^2)) (W/S) )^2
		Then substitute the equation for dynamic pressure in cruise (q_c= 1/2 rho_c V_c ^2) :
		C_D = C_{D,0} + kappa ( (1/qc) ( ( W/S  )^2 (Equation A) :
		Then equate the equation for $C_D$, assuming thrust equals drag, with equation (A)
		and substituting the equation for dynamic pressure:
		(T_{SL})/(q_c S) = C_{D,0} + kappa ( (1)/(q_c) (W/S) )^2
		Rearrange to put T_{SL} as the subject:
		T_{SL} = q_{c}S ( C_{D,0} + kappa ( (1/q_c) (W/S) )^{2} )
		Expand:
		T_{SL} = C_{D,0}(q_c S) + (kappa)/(q_c) (W^{2}/S)
		Divide by W to get (T/W) :
		(T_{SL}/W) = (C_{D,0} q_c )/( (W/S) + (kappa)/(q_c) (W/S)
		Considering that thrust in cruise is different to at sea level due to thrust lapse:
		(T/W)= (1/alpha) ( (C_{D,0} q_c)/(W/S)+(kappa/q_c)(W/S) )
		Noting that the relevant mass is W_0 in the constraints analysis, we use the weight fraction equation:
		(T/W)=(beta/alpha) ( (C_{D,0} q_c)/( beta (W/S) + (beta kappa)/(q_c) (W/S) )" />
    </div>
</div>

    <a class="btn btn-lg btn-primary" href="/constraints/compare" role="button">Start Task 1 &raquo;</a>
</div>

<div class="col-md-3" role="complementary">
    <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix-top">
        <ul class="nav bs-docs-sidenav">
            <li> <a href="#wdwm">What do we mean by Constraints Analysis?</a>
                <ul class="nav">
                    <li> <a href="#whytwwl">Why do you think that care about the aircraft's Thrust to Weight ratio and
                        Wing Loading?</a></li>
                </ul>
            </li>
            <li> <a href="#kcca">What are the key constraints for civilian aircraft design?</a></li>
            <li> <a href="#whereaircraftonconstraints">Where on a Constraints Analysis would you typically
                expect to find different aircraft?</a></li>
            <li> <a href="#keq">What are the key Constraint Analysis equations?</a>
                <ul class="nav">
                    <li> <a href="#weau">Which equations are used to derive the Constraint Analysis equations?</a></li>
                </ul>
            </li>
            <li> <a href="#derive">How do we derive a Constraint Analysis equation?</a></li>
            <li> <a href="/constraints/compare">Skip to exercises</a></li>
        </ul> <a class="back-to-top" href="#top"> Back to top </a>
    </nav>
</div><!-- Secondary column -->