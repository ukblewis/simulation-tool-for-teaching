var express = require('express');
var router = express.Router();

/* Home page */
router.get('/', function(req, res, next) {
    res.render('index', {path: 'introduction', title: 'Welcome to PedagogicalSim', systemInfo: systemInfo(res)
    });
});

/* Student flying playground page */
router.get('/fly', function(req, res, next) {
    res.render('fly', {path: 'fly', title: 'Ready?', systemInfo: systemInfo(res) });
});

/* Development page route, this page demonstrates the functionality available */
router.get('/development', function(req, res, next) {
    res.render('development', {path: 'development', title: 'Developer tools',
        systemInfo: systemInfo(res) });
});

/* Constraints Analysis pages */
router.get('/constraints/intro', function(req, res, next) {
    res.render('constraints/intro', {path: 'constraints/intro', title: 'Constraints Analysis: Introduction',
        systemInfo: systemInfo(res) });
});

router.get('/constraints/compare', function(req, res, next) {
    res.render('constraints/compare', {path: 'constraints/compare', title: 'Constraints Analysis: Task 1',
        systemInfo: systemInfo(res) });
});

router.get('/constraints/driving', function(req, res, next) {
    res.render('constraints/driving', {path: 'constraints/driving', title: 'Constraints Analysis: Task 2',
        systemInfo: systemInfo(res) });
});

/* Joystick pages */
router.get('/joystick/saitek', function(req, res, next) {
    res.render('joystick/saitek', {path: 'joystick/saitek', title: 'How to use the Saitek X52 Pro in FlightGear with ' +
    'PedagogicalSim', systemInfo: systemInfo(res) });
});

/* Reset route */
router.get('/reset', function(req, res, next) {
    process.exit();
});

/**
 * Helper function to turn file size in bytes into readable string
 * @param bytes The file size in bytes
 * @param {boolean} si Boolean indicating if SI units are preferred
 * @returns {string} String of the file size with the relevant postfix (e.g. MB or GB)
 */
function humanFileSize(bytes, si) {
    var thresh = si ? 1000 : 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si
        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1)+' '+units[u];
}

/* Helper function get the system IP */
const os = require('os');

var interfaces = os.networkInterfaces();
var addresses;
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses = address.address;
        }
    }
}

/**
 * Helper function to gather system info
 * @returns {string} System info string
 */
 function systemInfo() { return "OS kernel: "+os.type()+"; Release: "+os.release()+"; RAM: "
    +humanFileSize(os.totalmem().toString(),true)+"; CPU: "+os.cpus()[0].model+"; Architecture: "+os.arch()+
    "; Local IP: "+ addresses};

module.exports = router;
