/**
 * Aircraft List
 * @returns {Object} List of available aircraft models and flight conditions available,
 * with relevant speeds and altitudes for this aircraft and flight conditions
 */
module.exports = function() {
    return {
        "Aircraft1": {
            climb: {
                altitude: 10000,
                vc: 280
            },
            cruise: {
                altitude: 35000,
                heading: "090",
                offset: 15,
                vc: 260
            },
            landing: {
                altitude: 3000,
                vc: 180
            }
        },
        "Aircraft2": {
            takeoff: {
                altitude: 0,
                heading: "090",
                offset: 0,
                vc: 0
            },
            climb: {
                altitude: 10000,
                vc: 280
            },
            cruise: {
                altitude: 40000,
                heading: "090",
                offset: 15,
                mach: 1.6
            },
            landing: {
                altitude: 3000,
                vc: 180
            }
        },
        "dhc3": {
            takeoff: {
                altitude: 0,
                heading: "090",
                offset: 0,
                vc: 0
            }
        }
    }
};