/* Error handling and throttle/joystick position visualising */
var error = "";
socket.on('errorData', function(data) {
    error += data.dataString+"<br>";
    $('#errorPanel .panel-body').html(error);
    $('#errorPanel').show();
});
var trimThrottle = "";
var throttlePos = "";
var elevatorPos = 0;
var aileronPos = 0;
socket.on('throttleData', function(data) {
    var FGBox = document.getElementsByClassName('fg-box');
    for (var i=0; i<FGBox.length; i++) {
        var box = FGBox[i];
        if (box.style.display != 'block') {
            box.style.display = 'block';
        }
    }
    trimThrottle = data.trimThrottle;
    throttlePos = data.throttlePos;
    elevatorPos = parseInt(data.elevatorPos);
    aileronPos = parseInt(data.aileronPos);
    var $trimThrottle = document.getElementById('trimThrottle');
    var $throttlePos = document.getElementById('posThrottle');
    var $point = document.getElementById('graphPoint');
    if (trimThrottle == throttlePos) {
        $trimThrottle.classList.add('progress-bar-success');
        $throttlePos.classList.add('progress-bar-success');
    }
    else {
        $trimThrottle.classList.remove('progress-bar-success');
        $throttlePos.classList.remove('progress-bar-success');
    }
    $trimThrottle.innerHTML = trimThrottle+'%';
    $trimThrottle.setAttribute('aria-valuenow',trimThrottle);
    $trimThrottle.style.width = trimThrottle+'%';
    $throttlePos.innerHTML = throttlePos+'%';
    $throttlePos.setAttribute('aria-valuenow',throttlePos);
    $throttlePos.style.width= throttlePos+'%';
    $point.style.top = 'calc('+(-elevatorPos/2-50)+'% - 5px)';
    $point.style.marginLeft = 'calc('+(aileronPos/2+50)+'% - 5px)';
});