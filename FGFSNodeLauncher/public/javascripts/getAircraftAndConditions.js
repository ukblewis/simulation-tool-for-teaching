/* Add function to capitalise first letter of strings */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/* Populate aircraft names and flight conditions */
socketEmit('getAircraftList');
socket.on('gotAircraftList', function(data) {
    data.forEach(function (aircraft) {
        $('#aircraft').append('<option value="'+aircraft+'">'+aircraft+'</option>');
        $('select').selectpicker('refresh');
    });
    socketEmit('getCondition',document.getElementById('aircraft').value);
    document.getElementById('aircraft').addEventListener('change', function(e) {
        socketEmit('getCondition',this.value);
    });
});
socket.on('gotCondition', function(data) {
    $('#condition').children().remove();
    data.forEach(function (condition) {
        $('#condition').append('<option value="' + condition + '">' + condition.capitalize() + '</option>');
        $('select').selectpicker('refresh');
    });
});