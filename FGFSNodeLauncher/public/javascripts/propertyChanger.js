/* Property changer */
socketEmit('getPropertyList');
$('select').selectpicker();
socket.on('gotPropertyList', function(properties) {
    $('#properties').children().remove();
    for (var propertyName in properties) {
        $('#properties').append('<option value="'+properties[propertyName]+'">'+propertyName+'</option>');
    }
    $('select').selectpicker('refresh');
});
function getProp() {
    var prop = document.getElementById('properties').value;
    socketEmit('getProperty', {
        "property": prop
    });
}

socket.on('gotProperty', function(property) {
    document.getElementById('property-value').innerHTML = property;
});

function setProp() {
    var prop = document.getElementById('properties').value;
    var val = document.getElementById('propertyValue').value;
    socketEmit('setProperty', {
        "property": prop,
        "value": val
    });
}