/* Property list updater */
var selectedFile;
var propertyList = document.getElementById('propertyList');
propertyList.addEventListener('change', function(e) {
    selectedFile = e.target.files[0];
});

function sendProperties() {
    socketEmit('setPropertyList', selectedFile);
}

socket.on('propertyListSaved', function() {
    document.getElementById('propertyListSaved').style.display = 'block';
});